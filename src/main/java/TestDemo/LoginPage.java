package TestDemo;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author David Mamani C.
 */
public class LoginPage {
    /*@FindBy(id = "email")
    private WebElement emailTextField;

    @FindBy(css = "input.inputtext[name='text']")
    private WebElement passwordTextField;

    @FindBy(css = "input.inputtext[name='text']")
    private WebElement buttonb;*/

    @FindBy(id = "login_field")
    private WebElement loginTextField;

    @FindBy(id = "password")
    private WebElement passwordTextField;

    @FindBy(css = "input.btn.btn-primary")
    private WebElement singupButton;

    public void setUsername(String email){
        loginTextField.clear();
        loginTextField.sendKeys(email);
        loginTextField.sendKeys(Keys.TAB);
    }

    public void setPassword(String password){
        passwordTextField.clear();
        passwordTextField.sendKeys(password);
        passwordTextField.sendKeys(Keys.TAB);
    }

    public void clickSingup(){
        singupButton.click();
    }
}
