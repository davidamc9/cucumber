package TestRunner;

import java.io.File;

/**
 * @author David Mamani C.
 */
public class ResourceLoader {

    // retorna el archivo cargado
    public static File loadfile(String pathToFile){
        //ClassLoader classLoader = ResourceLoader.class.getClassLoader();
        //return new File(classLoader.getResource(pathToFile).getFile());
        String filePath = new File("src//test//resources//" + pathToFile).getAbsolutePath();
        return new File(filePath);
    }
}
