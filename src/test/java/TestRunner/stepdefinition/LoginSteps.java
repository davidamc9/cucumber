package TestRunner.stepdefinition;

import TestRunner.example.Login;
import TestRunner.example.TestBase;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * @author David Mamani C.
 */
public class LoginSteps {

    private WebDriver driver;
    private String user;
    private String password;
    private String url;
    private String browser;

    @Given("^necesitamos un usuario registrado$")
    public void necesitamos_un_usuario_registrado() throws Throwable {
        user="deskomunal";
        password="mund0libr3";
        System.out.println("Usuario registrado");
    }

    @Given("^inicializar el browser$")
    public void inicializar_el_browser() throws Throwable {
        TestBase testBase= PageFactory.initElements(driver,TestBase.class);
        testBase.Init();
        driver = testBase.getDriver();
        url = testBase.appURL;
        browser = testBase.browser;
    }

    @When("^entramos en la url$")
    public void entramos_en_la_url() throws Throwable {
        TestBase testBase= PageFactory.initElements(driver,TestBase.class);
        testBase.setDriver(browser,url);
    }

    @When("^click Signup$")
    public void click_Signup() throws Throwable {
        Login login=PageFactory.initElements(driver,Login.class);
        login.loginGitHub("deskomunal","mund0l1br3");
    }

    @Then("^login es exitoso$")
    public void login_es_exitoso() throws Throwable {
        System.out.println("login es exitoso");
    }

    //@After
    public void close(){
        TestBase testBase = PageFactory.initElements(driver,TestBase.class);
        testBase.tearDown(driver);
    }
}
