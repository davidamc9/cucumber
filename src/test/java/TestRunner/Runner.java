package TestRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * @author David Mamani C.
 * Paolo Lizarazu
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/feature"},
        format = {"pretty",
                  "html:build/cucumber",
                  "json:build/cucumber/cucumber.json",
                  "junit:build/test-report.xml"}
)
public class Runner {

}


/*@RunWith(Cucumber.class)
@CucumberOptions(
        features ="src/test/resources/feature"
)
public class Runner {
}*/
