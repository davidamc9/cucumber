package TestRunner.example;

import TestDemo.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * @author David Mamani C.
 */
public class Login {
    private WebDriver driver;

    public Login(WebDriver driver){
        this.driver = driver;
    }

    public void loginGitHub(String user, String password){
        LoginPage login = PageFactory.initElements(driver, LoginPage.class);
        login.setUsername(user);
        login.setPassword(password);
        login.clickSingup();
    }
}
