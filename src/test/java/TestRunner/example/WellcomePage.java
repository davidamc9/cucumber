package TestRunner.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author David Mamani C.
 */
public class WellcomePage {
    @FindBy(css = ".shelf-title")
    private WebElement label;

    public String getLabel(){
        return label.getText();
    }
}
