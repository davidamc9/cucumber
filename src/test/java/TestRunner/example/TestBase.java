package TestRunner.example;

import TestRunner.ResourceLoader;
import TestRunner.UtilityTest;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.Properties;

/**
 * @author David Mamani C.
 */
public class TestBase {
    private Properties props;
    public String appURL;
    public String browser;
    private WebDriver driver;

    @BeforeClass
    public void Init(){
        UtilityTest util = new UtilityTest();
        props = util.loadProperties();
        appURL = props.getProperty("ServerPath");
        browser = props.getProperty("Browser");

        setDriver(browser, appURL);
    }

    public void setDriver(String browser, String appURL){
        if(browser.equals("chrome")){
            driver = initChromeDriver(appURL);
        }else
        if(browser.equals("ie")){
            driver = initExplorer(appURL);
        }
    }

    public void tearDown(WebDriver driver){
        if (driver != null){
            driver.quit();
        }
    }

    private WebDriver initChromeDriver(String appURL){
        String path= ResourceLoader.loadfile("drivers/chromedriver.exe").getPath();
        //System.err.println("path: " + path);
        //System.err.println("appURL: " + appURL);
        System.setProperty("webdriver.chrome.driver", path);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(appURL);
        return driver;
    }

    private WebDriver initFirefox(String appURL){
        return driver;
    }

    private WebDriver initExplorer(String appURL){
        return driver;
    }

    public WebDriver getDriver() {
        return driver;
    }
}
