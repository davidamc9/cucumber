package TestRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author David Mamani C.
 */
public class UtilityTest {

    public Properties loadProperties(){
        Properties props = new Properties();
        String filePath = new File("src//test//resources//parameter.properties").getAbsolutePath();
        File file = new File(filePath);

        FileInputStream fileInput  = null;
        try {
            fileInput = new FileInputStream(file);
            props.load(fileInput);
        }catch (IOException ex){
            ex.printStackTrace();
        }finally {
            if (fileInput != null){
                try {
                    fileInput.close();
                }catch (IOException ex){
                    ex.printStackTrace();
                }
            }
        }
        return props;
    }
}
