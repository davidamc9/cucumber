$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "line": 1,
  "name": "Hacer login un login en la pagina GitHub",
  "description": "",
  "id": "hacer-login-un-login-en-la-pagina-github",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "Hacer login en la aplicacion",
  "description": "",
  "id": "hacer-login-un-login-en-la-pagina-github;hacer-login-en-la-aplicacion",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "necesitamos un usuario registrado",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "inicializar el browser",
  "keyword": "And "
});
formatter.step({
  "line": 5,
  "name": "entramos en la url",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "click Signup",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "login es exitoso",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.necesitamos_un_usuario_registrado()"
});
formatter.result({
  "duration": 285914039,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.inicializar_el_browser()"
});
formatter.result({
  "duration": 11066640987,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.entramos_en_la_url()"
});
formatter.result({
  "duration": 28731996718,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.click_Signup()"
});
formatter.result({
  "duration": 3593293095,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.login_es_exitoso()"
});
formatter.result({
  "duration": 197426,
  "status": "passed"
});
});